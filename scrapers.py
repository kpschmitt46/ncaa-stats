import requests
from itertools import chain
from bs4 import BeautifulSoup

def get_games(date):

    # API call to generate JSON data table with score information
    url = ('http://site.api.espn.com/apis/site/v2/sports/'
            'basketball/mens-college-basketball/scoreboard?'
            'lang=en&region=us&calendartype=blacklist&limit=300'
            '&dates={}')
            
    # Get JSON data
    response = requests.get(url.format(date)) 
    if not response.ok:
        return
    data = response.json()
    
    # Query each event in table for event ID
    ids = [d['id'] for d in data['events']]
    return ids


def get_play_by_play(game_id):
    
    # API call to generate JSON data table with play-by-play information
    url = ('http://cdn.espn.go.com/core/mens-college-basketball/'
            'playbyplay?gameId={}&xhr=1&render=true&device=desktop&userab=8')
            
    response = requests.get(url.format(game_id))
    if not response.ok:
        return

    # Parse HTML with play-by-play information
    html = response.json()['content']['html']
    soup = BeautifulSoup(html)

    # Extracts content from within play-by-play tables
    tables = soup.find_all('div', id=lambda x: x and x.startswith('gp-quarter-'))
    
    pbp = []
    rows = list(chain.from_iterable([t.find_all('tr') for t in tables]))
    for row in rows: # loop through all rows in play-by-play table
        try:
            pbp.append({
                'gametime': row.find('td', class_='time-stamp').text,
                'team': row.find_all('td')[1].img.get('src'),
                'description': row.find('td', class_='game-details').text,
                'score': row.find('td', class_='combined-score').text,
            })
        except AttributeError:
            continue
    return pbp
